# ~
# Thin wrapper around tween. Create fade in/out fast.
# Usage
# - set exported variables
# - adjust modulate color if desired
# - set start_on_ready to start fade on scene load
# - call start() otherwise

extends TextureRect

export(float) var fade_time = 0.5
export(bool) var start_on_ready = false
export(String, "in", "out") var mode = "out"

onready var tween = $Tween
var _modulate

signal fade_done(mode)

func start():
	match mode:
		"in":
			set_alpha(1.0)
			tween.interpolate_method(self, "set_alpha", 1.0, 0.0, fade_time, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
		"out":
			set_alpha(0.0)
			tween.interpolate_method(self, "set_alpha", 0.0, 1.0, fade_time, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	tween.start()

func _ready():
	tween.connect("tween_completed", self, "_tween_done")
	_modulate = get("modulate")
	
	if start_on_ready:
		start()

func set_alpha(alpha):
	_modulate.a = alpha
	set("modulate", _modulate)

func _tween_done(object, key):
	emit_signal("fade_done", mode)

