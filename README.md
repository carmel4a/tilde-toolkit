![logo](./icon.png)
# Tilde Toolkit

DRY utils for [Godot engine 3.x](https://godotengine.org/) written to
aid in prototyping games.

## Usage

Each subdirectory contains one module. Read code or docs if provided.
If hardcoded path is required follwing path is assumed

`res://addons/tilde-toolkit/subdirectory`

## Modules

|name       | description                                        |
|-----------|----------------------------------------------------|
|black_fade |fade in/out to given color. Useful for scene change.|

## License

[MIT License](./LICENSE.txt)
